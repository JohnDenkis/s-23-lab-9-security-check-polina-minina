# Test cases for stepik.org

**Tested cite: stepik.org**

## Test case 1 - Forgot password

Link to OWASP: https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#forgot-password-service

| Test step  | Result |
|---|---|
| Open stepik.org  | Ok |
| Push "Log in" button  | Ok, login window opened  |
| Push "Remind the password"  | Ok, form for entering email opened |
| Enter unexistent email and submit |  Message "The e-mail address is not assigned to any user account" was displayed ![img.png](img.png)|
| Enter existing email | Ok, text about sending recovery email was displayed and message was send to my email ![img_1.png](img_1.png)|

* Stepik shows different messages for existent and non-existent accounts. So, user emails can be found by brute force.
* There is no protections against excessive automated submissions such as a CAPTCHA

## Test case 2 - Email Address Validation

Link to OWASP: https://cheatsheetseries.owasp.org/cheatsheets/Input_Validation_Cheat_Sheet.html#email-address-validation

| Test step  | Result |
|---|---|
| Open stepik.org  | Ok |
| Push "Register" button  | Registration form opened  |
| Enter email without "@" sign  | Ok, warning message was shown ![img_2.png](img_2.png)|
| Enter email "test'`~@mail.ru that has normal sign "@" and several dangerous characters | Warning message was shown about double quotes only ![img_3.png](img_3.png)|
| Enter email test'`~@mail.ru that has normal sign "@" and several dangerous characters, but now without double quotes | New user was registered successfully, but I can not register such email on mail because it has wrong characters|
| Enter email qqqq...qqqq@mail.ru that has 150 characters "q" in the local part (before the @) | New user was registered successfully |

* Stepik checks that email has "@" sign, but it does not check existence of dangerous characters (such as backticks, single or double quotes, or null bytes). So, account can be registered with unexistent email.
* Stepik does not have limits on email local part length (no more that 63 characters). So, account can be registered with unexistent email.


## Test case 3 - File Upload Validation

Link to OWASP: https://cheatsheetseries.owasp.org/cheatsheets/File_Upload_Cheat_Sheet.html

| Test step  | Result |
|---|---|
| Open stepik.org  | Ok |
| Login to existing account  | Ok, user successfully is logged in |
| Clock to avatar and chose settings  | Ok, settings page is opened |
| Click to "Upload" button near the avatar and upload file with docx extension | Ok, message about wrong file type was shown. Interesting that before uploading, there is no information about required filetype and filesize ![img_4.png](img_4.png)|
| Click to "Upload" button near the avatar and upload file with extension jpg that was converted to jpg from docx  |  Ok, message about wrong file type was shown. ![img_4.png](img_4.png)|

* Stepik has protection against strange file extensions in avatar field. It checks for filetype and content of a file. 

## Test case 4 - XSS Filter Evasion

Link to OWASP: https://cheatsheetseries.owasp.org/cheatsheets/XSS_Filter_Evasion_Cheat_Sheet.html

| Test step  | Result |
|---|---|
| Open stepik.org  | Ok |
| Put into search field next text: `<SCRIPT SRC=https://cdn.jsdelivr.net/gh/Moksh45/host-xss.rocks/index.js></SCRIPT>`  | Ok, unsuccessful search message is shown, script was not executed  ![img_5.png](img_5.png)|
| Put into comments field next text: `<SCRIPT SRC=https://cdn.jsdelivr.net/gh/Moksh45/host-xss.rocks/index.js></SCRIPT>`  | Ok, comment was published as plain text, script was not executed  ![img_6.png](img_6.png)|

* Stepik has protection against XSS injections


## Test case 5 - HTTP headers

Link to OWASP: https://cheatsheetseries.owasp.org/cheatsheets/HTTP_Headers_Cheat_Sheet.html

| Test step  | Result |
|---|---|
| Open stepik.org  | Ok |
| Open dev tool section Network  | Ok, panel was opened |
| Reload page and look at Response Headers | Many important headers was not sent  ![img_7.png](img_7.png)|

* Stepik does not send headers like X-Frame-Options, X-XSS-Protection, X-Content-Type-Options, 
Referrer-Policy, Access-Control-Allow-Origin
* However, stepik send headers like Content-Type, Set-Cookie, Strict-Transport-Security in appropriate way